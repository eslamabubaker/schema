<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "<?php echo $contact->type; ?>",
  "url": "<?php echo $contact->url; ?>",
  "name": "<?php echo $contact->name; ?>"

  ,"contactPoint": { [
          <?php
          foreach($contactPoints as $contactPoint){

          ?>
          {
          "@type": "ContactPoint",
          "telephone": "<?php echo $contactPoint->val; ?>",
          "contactOption" : "<?php echo $contactPoint->option; ?>",
          "contactType": "<?php echo $contactPoint->type; ?>"
          }
          <?php
            if (!($contactPoint === end($contactPoints))){ ?>
             ,
             <?php }?>
         <?php } ?>
    ]}
    , "sameAs": [
    <?php
    foreach($sameAses as $sameAs){
        ?>

        <?php echo $linkedin;?>,
        <?php echo $facebook; ?>,
        <?php echo $googlep; ?>,
        <?php echo $twitter; ?>,
        <?php echo $integram; ?>
    <?php }
    ?>
    ]
}
</script>
