<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "<?php echo $local_bus->type?>",
  "image": "<?php echo $local_bus->image_url; ?>",
  "@id": "<?php echo $local_bus->url; ?>",
  "name": "<?php echo $local_bus->name; ?>",
  "address": {
    "@type": "<?php echo $local_bus->address_type; ?>",
    "streetAddress": "<?php echo $local_bus->address_street; ?>",
    "addressLocality": "<?php echo $local_bus->address_local; ?>",
    "addressRegion": "<?php echo $local_bus->address_rigon; ?>",
    "postalCode": "<?php echo $local_bus->address_postal; ?>",
    "addressCountry": "<?php echo $local_bus->address_country; ?>"
  },
  "geo": {
    "@type": "GeoCoordinates",
    "latitude": <?php echo $local_bus->latitude; ?>,
    "longitude": <?php echo $local_bus->longitude; ?>
  },
  "url": "<?php echo $local_bus->url;?>",
  "telephone": "<?php echo $local_bus->tele; ?>",
  "openingHoursSpecification": [
<?php
    foreach($openingHours as $openingHour){
    ?>

    {
      "@type": "OpeningHoursSpecification",
      "dayOfWeek": [
        <?php
        foreach($days as $day){
            ?>
        "<?php echo $day->name; ?>"
      <?php
      if (!($day === end($days))){ ?>
            ,
      <?php } ?>
        <?php
            }
            ?>

      ],
      "opens": "<?php echo $openingHour->opened; ?>",
      "closes": "<?php echo $openingHour->closed; ?>"
    }
    <?php
    if (!($openingHour === end($openingHours))){

    ?>
        ,
    <?php
        }
        ?>
    <?php
    }
    ?>

  ],
  "menu": "<?php echo $local_bus->menu_url;?>"

}
</script>