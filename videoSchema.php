<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "VideoObject",
  "name": "<?php echo $vedio->title; ?>",
  "description": "<?php echo $vedio->desc; ?>",
  "thumbnailUrl": "<?php echo $vedio->thumb_url; ?>",
  "uploadDate": "<?php echo $vedio->created_at; ?>",
  "duration": "<?php echo $vedio->duration; ?>",
  "publisher": {
    "@type": "<?php echo $vedio->publisher_type;?>",
    "name": "<?php echo $vedio->publisher_name;?>",
    "logo": {
      "@type": "ImageObject",
      "url": "<?php echo $vedio->logo_url;?>",
      "width": <?php echo $vedio->logo_width?>,
      "height": <?php echo $vedio->logo_height; ?>
    }
  },
  "contentUrl": "<?php echo $vedio->url; ?>",
  "embedUrl": "<?php echo $vedio->embed_url; ?>",
  "interactionCount": "<?php echo $vedio->interaction;?>",
  "expires": "<?php echo $vedio->expire;?>"
}
</script>