<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "<?php echo $type; ?>",
  "mainEntityOfPage": {
    "@type": "WebPage",
    "@id": "<?php echo $base_url.'/news/'.$artical->id.'-'.$artical->seo_name; ?>"
  },
  "headline": "<?php echo $artical->title ?>",
  "image": {
    "@type": "ImageObject",
    "caption": "<?php echo $artical->img_caption; ?>",
    "url": "<?php echo $base_url.'/'.$height.'x'.$width.'/'.$artical->image; ?>",
    "height": <?php echo $height; ?>,
    "width": <?php echo $width; ?>
  },
  "datePublished": "<?php echo toIso8601($artical->created_at);?>",
  "dateModified": "<?php echo toIso8601($artical->updated_at); ?>",
  "author": {
    "@type": "<?php echo $author_type; ?>",
    "name": "<?php echo $author_username; ?>"
  },
   "publisher": {
    "@type": "<?php echo  $publisher_type; ?>",
    "name": "<?php echo $publisher_name; ?>",
    "logo": {
      "@type": "ImageObject",
      "url": "<?php echo $base_url.'/'.$logo_a; ?>",
      "width": <?php echo $logo_width; ?>,
      "height": <?php echo $logo_height;?>
  }
},
"description": "<?php echo $artical->seo_description; ?>"
}
</script>
