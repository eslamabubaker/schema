<script type="application/ld+json">
{
  "@context":"http://schema.org",
  "@type":"TelevisionChannel",
  "name": "<?php echo $tele->name; ?>",
  "broadcastChannelId":"<?php echo $tele->brod_ch_id; ?>",
  "broadcastServiceTier":"<?php echo $tele->brod_tear; ?>",
  "inBroadcastLineup":{
    "@type":"CableOrSatelliteService",
    "name": "Comcast"
  },
  "providesBroadcastService":{
    "@type":"BroadcastService",
    "name": "<?php echo $tele->name; ?>",
    "broadcastDisplayName": "<?php echo $tele->display_name;?>",
    "broadcastAffiliateOf":{
      "@type":"<?php echo $tele->broad_aff_type; ?>",
      "name":"<?php echo $tele->display_name; ?>"
    }
  }
}
</script>