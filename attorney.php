<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Attorney",
  "name" : "<?php echo $name; ?>",
  "alternateName" : "<?php echo $altName; ?>",
  "description" : "<?php echo $description; ?>",
  "url" : "<?php echo $url; ?>",
  "logo" : "<?php echo $logo; ?>",
  "email": "<?php echo $email; ?>",
  "telephone": "<?php echo $tele; ?>",
  "faxNumber": "<?php echo $fax; ?>",
  "sameAs" :{
    <?php echo $linkedin;?>,
    <?php echo $facebook; ?>,
    <?php echo $googlep; ?>,
    <?php echo $twitter; ?>,
    <?php echo $integram; ?>
  },
  "contactPoint": { [
          <?php foreach($contactPoints as $contactPoint){

            ?>
        {
        "@type": "ContactPoint",
        "telephone": "<?php echo $contactPoint->val; ?>",
          "contactOption" : "<?php echo $contactPoint->option; ?>",
          "contactType": "<?php echo $contactPoint->type; ?>"
          }
         <?php if (!($contactPoint === end($contactPoints))){ ?>
            ,
        <?php } ?>
    <?php } ?>
    ]},
    "address": {
    "@type": "PostalAddress",
    "addressLocality": "<?php echo $address->locale; ?>",
    "postalCode": "<?php echo $address->postal; ?>",
    "streetAddress": "<?php echo $address->street; ?>",
    "addressCountry": "<?php echo $address->country ?>",
    "addressRegion": "<?php echo $address->street; ?>"
    }

}
</script>