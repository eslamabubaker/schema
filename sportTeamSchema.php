<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "SportsTeam",
  "name": "<?php echo $team->name ?>",
  "email": "<?php echo $team->email; ?>",
  "address": {
    "@type": "PostalAddress",
    "addressLocality": "<?php echo $team->locale; ?>",
    "postalCode": "<?php echo $team->postal; ?>",
    "streetAddress": "<?php echo $team->street; ?>"
  },
  "member": [
  <?php
    foreach($members as $member){ ?>
        {
    "@type": "OrganizationRole",
    "member": {
      "@type": "Person",
      "name": "<?php echo $member->name; ?>"
    },
    "startDate": "<?php echo $member->startyear; ?>",
    "endDate": "<?php echo $member->endyear; ?>",
    "roleName": "<?php echo $member->rolename; ?>"
  }
    <?php
    if (!($member === end($members))){ ?>
        ,
    <?php }
        ?>
    <?php
        }
        ?>

  ]
}
</script>