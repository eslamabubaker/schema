<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "MusicPlaylist",
  "name": "<?php echo $name; ?>",
  "numTracks": "<?php echo $no ?>",
  "track": [
  <?php
     foreach($tracks as  $track){ ?>
    {
      "@type": "MusicRecording",
      "byArtist": "{{$track->artist}}",
      "duration": "{{toIso8601($track->duration)}}",
      "inAlbum": "{{$track->albumName}}",
      "name": "{{$track->name}}",
      "url": "{{$track->url}}"
    }
    <?php
    if (!($track === end($tracks))){ ?>
            ,
        <?php } ?>
    <?php } ?>
  ]
}
</script>