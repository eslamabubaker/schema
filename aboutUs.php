<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "<?php echo $type; ?>",
  "address": {
    "@type": "<?php echo $address->type; ?>",
    "addressLocality": "<?php echo $address->addressLocality; ?>",
    "postalCode": "<?php echo $address->postalCode; ?>",
    "streetAddress": "<?php echo $address->streetAddress; ?>"
  },
  "email": "<?php echo $email; ?>",
  "faxNumber": "<?php echo $fax; ?>",
  "member": {
        "@type": "OrganizationRole",
    "member": [
    <?php
    foreach($members as $member){
    ?>
    {
      "@type": "<?php echo $member->type; ?>",
      "name": "<?php echo $member->name; ?>"
    }
    <?php
    if (!($member === end($members))) { ?>
            ,
    <?php
    } ?>
    <?php }
    ?>

    ]
  },
   "name": "<?php echo $name; ?>",
  "telephone": "<?php echo $tele; ?>"
}
</script>