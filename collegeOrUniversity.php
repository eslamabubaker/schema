<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "CollegeOrUniversity",
  "name": "<?php echo $uni->name;?>",
  "sameAs": "<?php echo $uni->sam_url; ?>",
  "alumni": {
  [
  <?php
  foreach($alumnis as $alumni){
  ?>
  {
    "@type": "OrganizationRole",
    "alumni": {
      "@type": "Person",
       "name": "<?php echo $alumni->name; ?>",
      "sameAs": "<?php echo $alumni->same_as; ?>"
    },
    "startDate": "<?php echo $alumni->started; ?>"
    }
    <?php
    if (!($alumni === end($alumnis))) { ?>
        ,
    <?php } ?>
 <?php
         }
         ?>

    ]
  }
}
</script>